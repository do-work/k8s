variables:
  YAOOK_K8S_RELEASE_BRANCH_PREFIX: release/v
  YAOOK_K8S_RELEASE_PREPARE_BRANCH_PREFIX: release-prepare/v
  YAOOK_K8S_RELEASE_HOTFIX_BRANCH_PREFIX: hotfix/
  YAOOK_K8S_DEFAULT_BRANCH: devel

  REGEX_RELEASE_BRANCH_PREFIX: /^release\/v\S+$/
  REGEX_RELEASE_PREPARE_BRANCH_PREFIX: /^release-prepare\/v\S+$/
  REGEX_RELEASE_HOTFIX_BRANCH_PREFIX: /^hotfix\/\S+$/
  REGEX_RELEASE_HOTFIX_BASE_BRANCH_PREFIX: /^hotfix\/base\S+$/

# Pipelines run on devel branch, release-(prepare)-branches and merge requests
workflow:
  rules:
    - if: $CI_COMMIT_BRANCH == $YAOOK_K8S_DEFAULT_BRANCH
    - if: '$CI_COMMIT_BRANCH =~ $REGEX_RELEASE_PREPARE_BRANCH_PREFIX'
    - if: '$CI_COMMIT_BRANCH =~ $REGEX_RELEASE_BRANCH_PREFIX'
    - if: $CI_MERGE_REQUEST_EVENT_TYPE
    - if: $CI_PIPELINE_SOURCE == "schedule"

stages:
- lint
- hotfix-prepare
- cluster-tests
- diagnostic-tools
- release

include:
  - local: ci/hotfix-prepare.yaml
  - local: ci/cluster-tests.yaml
  - local: ci/diagnostic-tools.yaml
  - local: ci/release.yaml


# always lint on merge trains
.default_lint_rules:
  rules:
    - if: $CI_MERGE_REQUEST_EVENT_TYPE == "merge_train"
      when: on_success

tflint:
  image:
    name: "ghcr.io/terraform-linters/tflint:v0.50.3"
    entrypoint: ["/bin/sh", "-c"]
  stage: lint
  script:
    - tflint --chdir=terraform/
  tags:
    - docker
  rules:
    - !reference [.default_lint_rules, rules]
    - changes:
      - 'terraform/**/*'
      when: on_success
    - when: never

shellcheck:
  image: "${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/koalaman/shellcheck-alpine:v0.10.0"
  stage: lint
  script:
    - "find -iname '*.sh' '!' -ipath './jsonnet-sources/*/vendor/**' '!' -ipath './.git/**' -print0 | xargs -0 -- shellcheck -Calways"
  tags:
    - docker
  rules:
    - !reference [.default_lint_rules, rules]
    - changes:
      - '**/*.sh'
      when: on_success
    - when: never

yamllint:
  stage: lint
  tags:
    - docker
  image: registry.gitlab.com/pipeline-components/yamllint:latest
  script:
    - yamllint .
  rules:
    - !reference [.default_lint_rules, rules]
    - changes:
      - '**/*.{yaml,yml}'
      when: on_success
    - when: never

detect-vault-image:
  image: "registry.gitlab.com/yaook/images/k8s-ci/f1a:devel"
  stage: lint
  script:
    - bash ./actions/detect-vault-image.sh
  tags:
    - docker
  rules:
    - !reference [.default_lint_rules, rules]
    - when: on_success

ansible-lint:
  image: "registry.gitlab.com/yaook/images/k8s-ci/base:devel"
  stage: lint
  before_script:
    - poetry install --with ci --sync
  script:
    - "ansible-galaxy install -r ansible/requirements.yaml"
    - "ANSIBLE_ROLES_PATH=./k8s-base/roles:./k8s-managed-services/roles/:./k8s-service-layer/roles:./k8s-base/test-roles/:./k8s-service-layer/test-roles/:./k8s-managed-services/test-roles ansible-lint -c ci/lint/ansible-lint-conf --offline"
  tags:
    - docker
  rules:
    - !reference [.default_lint_rules, rules]
    - when: on_success

flake8:
  image: "registry.gitlab.com/yaook/images/k8s-ci/base:devel"
  before_script:
    - poetry install --with ci --sync
  script:
    - 'python3 -m flake8'
  stage: lint
  tags:
    - docker
  rules:
    - !reference [.default_lint_rules, rules]
    - changes:
      - '**/*.py'
      when: on_success
    - when: never

poetry-lock:
  image: "registry.gitlab.com/yaook/images/k8s-ci/base:devel"
  before_script:
    - poetry install --sync
    - git config --global user.email "$GITLAB_USER_EMAIL"
    - git config --global user.name "$GITLAB_USER_NAME"
  script:
    - bash ./ci/lint/poetry-lock.sh
  stage: lint
  tags:
    - docker
  rules:
    - !reference [.default_lint_rules, rules]
    - changes:
      - pyproject.toml
      - poetry.lock
      when: on_success
    - when: never
    # - when: on_success

pre-commit-hooks:
  image: "registry.gitlab.com/yaook/images/k8s-ci/base:devel"
  before_script:
    - poetry install --sync
  script:
    - 'SKIP=shellcheck,yamllint,flake8,check-flake pre-commit run --all-files || true'
    - git --no-pager diff
    - git restore .
    - 'SKIP=shellcheck,yamllint,flake8,check-flake pre-commit run --all-files'
  stage: lint
  tags:
    - docker
  rules:
    - !reference [.default_lint_rules, rules]
    - when: on_success

build-docs-check:
  stage: lint
  image: "registry.gitlab.com/yaook/images/k8s-ci/base:devel"
  before_script:
    - poetry install --with docs --sync
  script:
    - sphinx-build -W docs _build/html
    - mv _build/html public
  artifacts:
    expose_as: "Rendered Docs"
    expire_in: "7 days"
    paths:
      - public/
  rules:
    # run only on MR when changes were made
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE == "detached" || $CI_MERGE_REQUEST_EVENT_TYPE == "merged_result"'
      changes:
      - 'docs/[!_]*/**/*'
      - 'docs/[!_]*'  # the above does not include files directly in docs/
      when: on_success
    - when: never
  tags:
    - docker

release-note-file-check:
  stage: lint
  image: "registry.gitlab.com/yaook/images/k8s-ci/base:devel"
  variables:
    CONFIG_FILE: towncrier.toml
    HOTFIX: "False"
    SOURCE_BRANCH: origin/${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}
    TARGET_BRANCH: origin/${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}
    COMMIT_MESSAGE: MR-number for releasenote(s) changed
    COMMIT_FILES: docs/_releasenotes
  before_script:
    - poetry install --with ci --with docs --sync
    - git fetch origin "${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}"
    - |
      if [ "${CI_MERGE_REQUEST_PROJECT_URL}" = "${CI_MERGE_REQUEST_SOURCE_PROJECT_URL}" ]; then
        FORK=False
      else
        FORK=True
      fi
    - git config --global user.name "${GITLAB_USER_NAME}"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
  script:
    # we need to catch the exit-code from python as it otherwise will not be transmitted correctly
    - python3 ci/lint/check-releasenote-file.py "${CI_PROJECT_DIR}" "${TARGET_BRANCH}" "${CONFIG_FILE}" "${CI_MERGE_REQUEST_IID}" "${COMMIT_FILES}" "${FORK}" "${HOTFIX}"
    - exit_code=$?
    - towncrier build --version x.x.x --config "${CONFIG_FILE}" --draft
    - |
      if [ "${exit_code}" != 0 ]; then
        exit "${exit_code}"
      fi
    - |
      if [ "${FORK}" = "False" ]; then
        git fetch origin "${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}"
        git checkout "${SOURCE_BRANCH}"
        git status
        CHANGES=$(git diff "${SOURCE_BRANCH}" --name-only -- "${COMMIT_FILES}" | wc -l)
        if [ "${CHANGES}" -gt 0 ]; then
          echo "committing"
          git add ${COMMIT_FILES}
          git commit --amend --no-edit
          git push --force -o ci.skip "https://gitlab-ci-token:${PUSH_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}" HEAD:"${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}"
        fi
      fi
  tags:
    - docker
  retry: 1
  allow_failure:
    exit_codes: 13 # if number in hotfix doesn't match MR-IID, exit with warning
  rules:
    # don't run when CHANGELOG.rst has been edited
    - if:
      changes:
      - CHANGELOG.rst
      when: never
    # run only for MRs from hotfixing-branches ..
    - if: '$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ $REGEX_RELEASE_HOTFIX_BRANCH_PREFIX'
      variables:
        COMMIT_FILES: docs/_releasenotes/hotfix
        HOTFIX: "True"
      when: on_success
    # .. and to devel
    - if:  $CI_MERGE_REQUEST_EVENT_TYPE == "merged_result" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $YAOOK_K8S_DEFAULT_BRANCH
      when: on_success
    - when: never
