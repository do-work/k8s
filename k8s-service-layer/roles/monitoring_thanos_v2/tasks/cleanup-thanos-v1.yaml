---
- name: Load thanos-query deployment info
  kubernetes.core.k8s_info:
    api_version: apps/v1
    kind: Deployment
    name: thanos-query
    namespace: "{{ monitoring_namespace }}"
  register: thanos_query_deployment

- name: Delete old thanos-query deployment
  kubernetes.core.k8s:
    state: absent
    definition:
      apiVersion: apps/v1
      kind: Deployment
      metadata:
        name: "{{ item.metadata.name }}"
        namespace: "{{ item.metadata.namespace }}"
  when: (not 'app.kubernetes.io/managed-by' in item.metadata.labels) or
        (item.metadata.labels['app.kubernetes.io/managed-by'] != 'Helm')
  loop: "{{ thanos_query_deployment.resources }}"
  loop_control:
    label: "{{ item.metadata.name }}"

- name: Load thanos-query service info
  kubernetes.core.k8s_info:
    api_version: v1
    kind: Service
    name: thanos-query
    namespace: "{{ monitoring_namespace }}"
  register: thanos_query_service

- name: Delete old thanos-query service
  kubernetes.core.k8s:
    state: absent
    definition:
      apiVersion: v1
      kind: Service
      metadata:
        name: "{{ item.metadata.name }}"
        namespace: "{{ item.metadata.namespace }}"
  when: (not 'app.kubernetes.io/managed-by' in item.metadata.labels) or
        (item.metadata.labels['app.kubernetes.io/managed-by'] != 'Helm')
  loop: "{{ thanos_query_service.resources }}"
  loop_control:
    label: "{{ item.metadata.name }}"

- name: Load thanos-store service info
  kubernetes.core.k8s_info:
    api_version: v1
    kind: Service
    name: thanos-store
    namespace: "{{ monitoring_namespace }}"
  register: thanos_store_service

- name: Delete old thanos-store service
  kubernetes.core.k8s:
    state: absent
    definition:
      apiVersion: v1
      kind: Service
      metadata:
        name: "{{ item.metadata.name }}"
        namespace: "{{ item.metadata.namespace }}"
  when: (not 'app.kubernetes.io/managed-by' in item.metadata.labels) or
        (item.metadata.labels['app.kubernetes.io/managed-by'] != 'Helm')
  loop: "{{ thanos_store_service.resources }}"
  loop_control:
    label: "{{ item.metadata.name }}"

- name: Load thanos-compact service info
  kubernetes.core.k8s_info:
    api_version: v1
    kind: Service
    name: thanos-compact
    namespace: "{{ monitoring_namespace }}"
  register: thanos_compact_service

- name: Delete old thanos-compact service
  kubernetes.core.k8s:
    state: absent
    definition:
      apiVersion: v1
      kind: Service
      metadata:
        name: "{{ item.metadata.name }}"
        namespace: "{{ item.metadata.namespace }}"
  when: (not 'app.kubernetes.io/managed-by' in item.metadata.labels) or
        (item.metadata.labels['app.kubernetes.io/managed-by'] != 'Helm')
  loop: "{{ thanos_compact_service.resources }}"
  loop_control:
    label: "{{ item.metadata.name }}"

- name: Load thanos-compact statefulset info
  kubernetes.core.k8s_info:
    api_version: apps/v1
    kind: StatefulSet
    name: thanos-compact
    namespace: "{{ monitoring_namespace }}"
  register: thanos_compact_statefulset

- name: Delete old thanos-compact statefulset
  kubernetes.core.k8s:
    state: absent
    definition:
      apiVersion: v1
      kind: StatefulSet
      metadata:
        name: "{{ item.metadata.name }}"
        namespace: "{{ item.metadata.namespace }}"
  when: (not 'app.kubernetes.io/managed-by' in item.metadata.labels) or
        (item.metadata.labels['app.kubernetes.io/managed-by'] != 'Helm')
  loop: "{{ thanos_compact_statefulset.resources }}"
  loop_control:
    label: "{{ item.metadata.name }}"

- name: Load thanos-store statefulset info
  kubernetes.core.k8s_info:
    api_version: apps/v1
    kind: StatefulSet
    name: thanos-store
    namespace: "{{ monitoring_namespace }}"
  register: thanos_store_statefulset

- name: Delete old thanos-store statefulset
  kubernetes.core.k8s:
    state: absent
    definition:
      apiVersion: v1
      kind: StatefulSet
      metadata:
        name: "{{ item.metadata.name }}"
        namespace: "{{ item.metadata.namespace }}"
  when: (not 'app.kubernetes.io/managed-by' in item.metadata.labels) or
        (item.metadata.labels['app.kubernetes.io/managed-by'] != 'Helm')
  loop: "{{ thanos_store_statefulset.resources }}"
  loop_control:
    label: "{{ item.metadata.name }}"

- name: Delete thanos ServiceMonitors
  kubernetes.core.k8s:
    state: absent
    kind: ServiceMonitor
    api_version: monitoring.coreos.com/v1
    name: "{{ item }}"
    namespace: "{{ monitoring_namespace }}"
  loop:
  - thanos-compact
  - thanos-query
  - thanos-store
...
