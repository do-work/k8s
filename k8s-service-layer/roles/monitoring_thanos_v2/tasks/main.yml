---
- name: Determine Thanos object storage configuration
  block:
  - name: Read S3 config from Vault
    when: not monitoring_manage_thanos_bucket | bool
    environment:
      ANSIBLE_HASHI_VAULT_URL: "{{ lookup('env', 'VAULT_ADDR') }}"
      ANSIBLE_HASHI_VAULT_CA_CERT: "{{ lookup('env', 'VAULT_CACERT') }}"
    vars:
      vault_auth_mount_point: "{{ lookup('env', 'VAULT_AUTH_PATH') }}"
      vault_auth_method: "{{ lookup('env', 'VAULT_AUTH_METHOD') or (lookup('env', 'VAULT_TOKEN') | ternary('token', 'approle')) }}"
      vault_token: "{{ lookup('env', 'VAULT_TOKEN') }}"
      vault_role_id: "{{ lookup('env', 'VAULT_ROLE_ID') }}"
      vault_secret_id: "{{ lookup('env', 'VAULT_SECRET_ID') }}"
      thanos_config_vault_mount_point: "yaook/{{ vault_cluster_name }}/kv"
    ansible.builtin.set_fact:
      thanos_object_storage_config: "{{ lookup('community.hashi_vault.vault_kv2_get', 'thanos-config', engine_mount_point=thanos_config_vault_mount_point, mount_point=vault_auth_mount_point, auth_method=vault_auth_method, token=vault_token, role_id=vault_role_id, secret_id=vault_secret_id).data.data }}"

  - name: Use cluster OpenStack project for Thanos object storage configuration
    when: monitoring_manage_thanos_bucket | bool
    block:
    - name: Are OpenStack credentials available?
      include_role:
        name: check-openstack-credentials

    - name: Set Thanos object storage configuration for this clusters OpenStack project
      ansible.builtin.set_fact:
        thanos_object_storage_config: "{{ lookup('template', 'thanos-objectstorage.yaml.j2') }}"

- name: Create namespace
  kubernetes.core.k8s:
    apply: true
    definition:
      apiVersion: v1
      kind: Namespace
      metadata:
        name: "{{ monitoring_namespace }}"
    validate:
      fail_on_error: true
      strict: true
  # Retry this task on failures
  register: k8s_apply
  until: k8s_apply is not failed
  retries: "{{ k8s_error_retries }}"
  delay: "{{ k8s_error_delay }}"
  tags:
  - monitoring
  - thanos

- name: Cleanup K8s resource for the thanos v1 deployment
  block:
  - name: Check if Thanos v1 is deployed
    kubernetes.core.k8s_info:
      kind: Pod
      api_version: v1
      namespace: "{{ monitoring_namespace }}"
      label_selectors:
      - app.kubernetes.io/version=v0.26.0
    register: k8s_get_thanos_v1
    until: k8s_get_thanos_v1 is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"
    tags:
    - thanos

  - name: Include Thanos v1 cleanup tasks
    ansible.builtin.include_tasks: cleanup-thanos-v1.yaml
    when: k8s_get_thanos_v1.resources is defined and k8s_get_thanos_v1.resources | length != 0
    tags:
    - thanos

- name: Create storage bucket secret for thanos sidecar
  kubernetes.core.k8s:
    apply: true
    definition:
      apiVersion: v1
      type: Opaque
      kind: Secret
      metadata:
        name: thanos-sidecar-bucket-credentials-config
        namespace: "{{ monitoring_namespace }}"
      data:
        thanos.yaml: "{{ thanos_object_storage_config | b64encode }}"
    validate:
      fail_on_error: true
      strict: true
  # Retry this task on failures
  register: k8s_apply
  until: k8s_apply is not failed
  retries: "{{ k8s_error_retries }}"
  delay: "{{ k8s_error_delay }}"
  when: monitoring_use_thanos

- name: Create thanos secret
  kubernetes.core.k8s:
    apply: true
    definition:
      apiVersion: v1
      type: Opaque
      kind: Secret
      metadata:
        name: "{{ monitoring_thanos_config_secret_name }}"
        namespace: "{{ monitoring_namespace }}"
      data:
        objstore.yml: "{{ thanos_object_storage_config | b64encode }}"
    validate:
      fail_on_error: true
      strict: true
  # Retry this task on failures
  register: k8s_apply
  until: k8s_apply is not failed
  retries: "{{ k8s_error_retries }}"
  delay: "{{ k8s_error_delay }}"
  tags:
  - thanos

- name: "{{ monitoring_use_thanos | ternary('Add', 'Remove') }} bitnami Repo"
  kubernetes.core.helm_repository:
    name: bitnami
    repo_url: "{{ monitoring_thanos_helm_repo_url }}"
    repo_state: "{{ monitoring_use_thanos | ternary('present', 'absent') }}"
    force_update: true
  # Retry this task on failures
  register: task_result
  until: task_result is not failed
  retries: "{{ network_error_retries }}"
  delay: "{{ network_error_delay }}"
  tags:
  - thanos

- name: "{{ monitoring_use_thanos | ternary('I', 'Uni') }}nstall Thanos helm chart"
  vars:
    scheduling_key: "{{ monitoring_scheduling_key }}"
  kubernetes.core.helm:
    chart_ref: "{{ monitoring_thanos_chart_ref }}"
    chart_version: "{{ monitoring_thanos_chart_version }}"
    release_namespace: "{{ monitoring_namespace }}"
    release_name: "{{ monitoring_thanos_release_name }}"
    release_state: "{{ monitoring_use_thanos | ternary('present', 'absent') }}"
    values: "{{ lookup('template', 'thanos_values.yaml.j2') | from_yaml }}"
    wait: true
    update_repo_cache: true
  # Retry this task on failures
  register: task_result
  until: task_result is not failed
  retries: "{{ network_error_retries }}"
  delay: "{{ network_error_delay }}"
  tags:
  - thanos
...
