# This upgrade file is only for the k8s version 1.27.
# We keep for every minor update a separate upgrade file
# to avoid accidental major changes of old upgrade files.
---
- name: Detect user
  hosts: k8s_nodes
  gather_facts: false
  # we need *all* hosts.
  any_errors_fatal: true
  vars_files:
    - ../vars/etc.yaml
  roles:
    - detect_user

- name: Run pre-flight checks on all nodes
  hosts: k8s_nodes
  gather_facts: true
  vars_files:
    - ../vars/retries.yaml
  roles:
    - kubernetes-upgrade-info
  tags:
    - kubernetes-upgrade-preflight-checks

- name: Renew certificates (CP)
  hosts: masters
  gather_facts: true
  vars_files:
    - ../vars/disruption.yaml
    - ../vars/auto_generated_preamble.yaml
    - ../vars/etc.yaml
    - ../vars/retries.yaml
  serial:
    - 1
    - "100%"
  roles:
    - role: k8s-master
      tags: k8s-master

- name: Renew certificates (Worker)
  hosts: workers
  gather_facts: true
  vars_files:
    - ../vars/disruption.yaml
    - ../vars/auto_generated_preamble.yaml
    - ../vars/etc.yaml
    - ../vars/retries.yaml
  roles:
    - role: k8s-worker
      tags: k8s-worker

- name: Upgrade the first master node
  hosts: masters[0]
  gather_facts: false
  any_errors_fatal: true
  vars_files:
    - ../vars/disruption.yaml
    - ../vars/retries.yaml
    - ../vars/etc.yaml
    - ../vars/auto_generated_preamble.yaml
  tags:
    - kubeadm-first-master
  pre_tasks:
    - name: include ksl vars
      when: ksl_vars_directory is defined
      ansible.builtin.include_vars:
        dir: "{{ ksl_vars_directory }}"
      tags:
        - always
  roles:
    - role: cluster-health-verification
      delegate_to: localhost
      when: not (k8s_skip_upgrade_checks | bool) and not (k8s_upgrade_done | default(False))
    - role: kubeadm-drain-node
      when: "not (k8s_upgrade_done | default(False))"
    - role: update_system
      when: "not (k8s_upgrade_done | default(False))"
    - role: kubeadm-upgrade-kubeadm
      when: "not (k8s_upgrade_done | default(False))"
    - role: kubeadm-patches
      when: "not (k8s_upgrade_done | default(False))"
    - role: kubeadm-upgrade-apply
      when: "not (k8s_upgrade_done | default(False))"
    - role: kubeadm-uncordon-node
      when: "not (k8s_upgrade_done | default(False))"

- name: Upgrade the residual master nodes
  hosts: masters[1:]
  gather_facts: false
  serial: 1
  any_errors_fatal: true
  vars_files:
    - ../vars/disruption.yaml
    - ../vars/retries.yaml
    - ../vars/auto_generated_preamble.yaml
  tags:
    - kubeadm-other-masters
  pre_tasks:
    - name: include ksl vars
      when: ksl_vars_directory is defined
      ansible.builtin.include_vars:
        dir: "{{ ksl_vars_directory }}"
      tags:
        - always
  roles:
    - role: cluster-health-verification
      delegate_to: localhost
      when: not (k8s_skip_upgrade_checks | bool) and not (k8s_upgrade_done | default(False))
    - role: kubeadm-drain-node
      when: "not (k8s_upgrade_done | default(False))"
    - role: update_system
      when: "not (k8s_upgrade_done | default(False))"
    - role: kubeadm-upgrade-kubeadm
      when: "not (k8s_upgrade_done | default(False))"
    - role: kubeadm-patches
      when: "not (k8s_upgrade_done | default(False))"
    - role: kubeadm-upgrade-node
      when: "not (k8s_upgrade_done | default(False))"
    - role: kubeadm-uncordon-node
      when: "not (k8s_upgrade_done | default(False))"

- name: Upgrade kubelet on master nodes
  hosts: masters
  gather_facts: false
  any_errors_fatal: true
  tags:
    - kubelet-masters
  serial: 1
  vars_files:
    - ../vars/retries.yaml
    - ../vars/auto_generated_preamble.yaml
  roles:
    - role: kubeadm-upgrade-kubelet
      when: "not (k8s_upgrade_done | default(False))"
      # we do not want to customize kubelet on control-plane nodes
      vars:
        k8s_kubelet_disable_customizations: true

- name: Upgrade the worker nodes
  hosts: workers
  gather_facts: false
  any_errors_fatal: true
  vars_files:
    - ../vars/disruption.yaml
    - ../vars/retries.yaml
    - ../vars/auto_generated_preamble.yaml
  tags:
    - workers
  serial: 1
  pre_tasks:
    - name: include ksl vars
      when: ksl_vars_directory is defined
      ansible.builtin.include_vars:
        dir: "{{ ksl_vars_directory }}"
      tags:
        - always
  roles:
    - role: cluster-health-verification
      delegate_to: localhost
      when: not (k8s_skip_upgrade_checks | bool) and not (k8s_upgrade_done | default(False))
    - role: kubeadm-drain-node
      when: "not (k8s_upgrade_done | default(False))"
    - role: update_system
      when: "not (k8s_upgrade_done | default(False))"
    - role: kubeadm-upgrade-kubeadm
      when: "not (k8s_upgrade_done | default(False))"
    - role: kubeadm-upgrade-node
      when: "not (k8s_upgrade_done | default(False))"
    - role: kubeadm-upgrade-kubelet
      when: "not (k8s_upgrade_done | default(False))"
  # The nvidia-device-plugin marks a GPU as unhealthy
  # on systemctl daemon-reloads + kubelet restarts,
  # but the Pod does not fail. We have to manually restart the Pod.
  tasks:
    - name: Restart nvidia-device-plugin Pod on node
      when: k8s_is_gpu_cluster | bool
      ansible.builtin.include_role:
        name: nvidia-device-plugin
        tasks_from: restart_ndp_pod.yaml
    - name: Uncordon the node
      ansible.builtin.include_role:
        name: kubeadm-uncordon-node
      when: "not (k8s_upgrade_done | default(False))"

- name: Update volume snapshot controller
  hosts: masters
  gather_facts: false
  vars_files:
    - ../vars/retries.yaml
  pre_tasks:
    - name: Overwrite k8s_version with next_k8s_version
      ansible.builtin.set_fact:
        k8s_version: "{{ next_k8s_version }}"
  roles:
    - role: volume-snapshots
      tags:
        - volume-snapshot
        - volume-snapshot-controller

- name: Update OpenStack cloud provider integration components
  hosts: masters
  run_once: true  # to avoid redundant appliance
  vars_files:
    - ../vars/auto_generated_preamble.yaml  # needed for the cloud.config secret
    - ../vars/retries.yaml
  pre_tasks:
    - name: Overwrite k8s_version with next_k8s_version
      ansible.builtin.set_fact:
        k8s_version: "{{ next_k8s_version }}"
  roles:
    - role: connect-k8s-to-openstack
      when: on_openstack | default(False) | bool

# Note that we're force migrating here
- name: Update calico with the tigera operator
  hosts: masters
  gather_facts: false
  vars:
    k8s_version: "{{ next_k8s_version }}"
  vars_files:
    - ../vars/disruption.yaml
    - ../vars/auto_generated_preamble.yaml
    - ../vars/etc.yaml
    - ../vars/retries.yaml
  pre_tasks:
    - name: Overwrite k8s_version with next_k8s_version
      ansible.builtin.set_fact:
        k8s_version: "{{ next_k8s_version }}"
  roles:
    - role: calico-tigera-operator
      tags:
        - calico

- name: Update ch-k8s-lbaas-controller
  hosts: masters
  vars_files:
    - ../vars/retries.yaml
  run_once: true
  roles:
    - role: ch-k8s-lbaas-controller
      tags:
        - ch-k8s-lbaas
        - ch-k8s-lbaas-controller

- name: Final upgrade note
  gather_facts: false
  hosts: masters[0]
  tasks:
    - name: Final upgrade note
      delegate_to: localhost
      ansible.builtin.debug:
        msg: >
          Congratulations!

            \o/     The upgrade to
             |      {{ next_k8s_version }}
            / \     is complete.

          You MUST now change the k8s_version variable to
          {{ next_k8s_version | to_json }}.
...
